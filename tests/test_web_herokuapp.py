import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from pages.herokuapp import HerokuAppMainPage


@pytest.fixture(scope='function')
def browser() -> WebDriver:
    driver = webdriver.Remote(
        command_executor='http://selenium__standalone-chrome:4444/wd/hub',
        options=Options())
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


def test_right_click_shows_the_internet(browser):
    # load heroku app main page and look for appearence of specific phrase inside paragraphs
    main_page = HerokuAppMainPage(browser)
    main_page.load()
    PHRASE = 'Right-click in the box below to see one called \'the-internet\''
    assert main_page.phrase_result_count(PHRASE) > 0


def test_alibaba_exist_in_page(browser):
    # load heroku app main page and look for appearence of specific phrase inside paragraphs
    main_page = HerokuAppMainPage(browser)
    main_page.load()
    PHRASE = 'Alibaba'
    assert main_page.phrase_result_count(PHRASE) > 0
