from selenium.webdriver.common.by import By

class HerokuAppMainPage:
  URL = 'https://the-internet.herokuapp.com/context_menu'

  def __init__(self, browser):
    self.browser = browser

  def load(self):
    self.browser.get(self.URL)

  def phrase_result_count(self, phrase):
    elements = self.browser.find_elements(By.TAG_NAME, 'p')
    p_content = [c.text for c in elements if phrase in c.text]
    return len(p_content)
