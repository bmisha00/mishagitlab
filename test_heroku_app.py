# MIT License
#
# Copyright (c) 2020 Aleksandr Kotlyar
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


@pytest.fixture(scope='function')
def browser() -> WebDriver:
    driver = webdriver.Remote(
        command_executor='http://selenium__standalone-chrome:4444/wd/hub',
        options=Options())
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


def test_right_click_shows_the_internet(browser):
    URL = 'https://the-internet.herokuapp.com/context_menu'
    PHRASE = 'Right-click in the box below to see one called \'the-internet\''
    browser.get(URL)
    elements = browser.find_elements(By.TAG_NAME, 'p')
    p_content = [c.text for c in elements if PHRASE in c.text]
    assert len(p_content) > 0

def test_alibaba_exist_in_page(browser):
    URL = 'https://the-internet.herokuapp.com/context_menu'
    PHRASE = 'Alibaba'
    browser.get(URL)
    elements = browser.find_elements(By.TAG_NAME, 'p')
    p_content = [c.text for c in elements if PHRASE in c.text]
    assert len(p_content) > 0
